---
title: ubuntu 20.04 增强功能安装 网络设置及其他
date: 2021-03-23 14:06:55
author: iattach
avatar: avatar.jpg
authorLink: hexo.iattach.top
categories: 技术
tags:
keywords: ubuntu,网络设置，增强功能，问题
---

# 安装增强功能

因为cuda和cudnn的安装，ubuntu虚拟机的占用空间指数级增加。所以只好删了重装虚拟机，因为安装 virtual box 增强功能出现以下错误 Could not mount the media/drive 'D:\Program Files\Oracle\VirtualBox/VBoxGuestAdditions.iso' (VERR_PDM_MEDIA_LOCKED)。记录下处理步骤及后续处理，事实上ubuntu安装的时候，virtualbox就已经内置了一个虚拟光驱 VBox_GAs_6.1.18 ，所以执行脚本应该可以。先在vbox光驱 执行命令 

sudo ./autorun.sh 

提示需要重启 启用kernel 于是重启

刚才同时提示需要装gcc和其他一些东西， ok 先安装

sudo apt-get update

sudo apt-get install build-essential gcc make perl dkms

reboot重启

sudo ./autorun.sh 

reboot重启解决

因为这个装好了 分辨率也可以选自动调整窗口大小 分辨率也就不用管了

# 黑屏时间较长 共享文件夹的问题

但是重启的过程中发现 黑屏时间较长 网上找了下是共享文件夹的问题 确实 没办法打开sf_ubuntu

若在设置共享目录时勾选了“自动挂载”，则guest中的Linux里什么都不用动，系统会自动在“/media”里建立以“sf_”为前缀并加上共享名的挂载点，比如你设置的共享名是“share”，那么这个挂载点就是“sf_share”，系统一启动就自动挂载好了。不过因为这个挂载点默认的权限是给vbox创建的用户组“vboxsf”的，你会无法查看，只需要修改“/etc/group”，把自己的用户名加入到“vboxsf”组就可以了。若这种情况下你又去手动修改“fstab”以实现自动挂载当然会失败。所以，若想通过“fstab”自动挂载，就要关掉共享目录设置中的“自动挂载”。注意 改的话 要用sudo 否则 没权限不能改

# 桌面快捷方式

个人比较懒 给挂载的共享文件夹 再创个桌面快捷方式

ln -s "/media/sf_ubuntu" ~/Desktop

# 主机和虚拟机共享同一局域网

最后解决下主机和虚拟机共享同一网络的问题

网卡1，使用 仅主机(Host-Only)适配器 的连接方式，完成 即：从主机可以通过一个静态IP访问到每一个虚拟机，从虚拟机中可以访问主机（主机也有一个固定的静态IP）ubuntu 下 需要 ifconfig windows 需要ipconfig 可以确认两者处于同一局域网中

如果要上网的话 另一个网卡设置nat网络转发就可以

# ubuntu查看已安装所有软件包并卸载

dpkg -l --列出当前系统中所有的包.可以和参数less一起使用在分屏查看. (类似于rpm -qa)
dpkg -l |grep -i "软件包名" --查看系统中与"软件包名"相关联的包.

sudo apt-get remove .... --purge 

