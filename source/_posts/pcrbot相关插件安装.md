---
title: pcrbot相关插件安装
author: iattach
avatar: 'https://wx1.sinaimg.cn/large/006bYVyvgy1ftand2qurdj303c03cdfv.jpg'
authorLink: hexo.iattach.top
categories: 技术
comments: true
copyright: false
date: 2021-12-31 16:53:54
authorAbout:
authorDesc:
tags:
keywords:
description:
photos:
---

# 聊天词云生成

仓库地址：[https://github.com/A-kirami/GroupWordCloudGenerator](https://github.com/A-kirami/GroupWordCloudGenerator)

安装依赖错误：
```bash
error: Microsoft Visual C++ 14.0 or greater is required. Get it with "Microsoft C++ Build Tools"
```

待处理

# 贵族决斗

仓库地址：[https://github.com/Rs794613/PcrDuel]()

DLC文件参考ReadME,unit文件夹在res下，路径参考：res/img/priconne/unit

因为hoshino_xcw嵌入了宝石银行，将宝石转换为金币部分代码：
```python
gid = ev.group_id
uid = ev.user_id
score_counter = ScoreCounter2()

jewel_counter = jewel.jewelCounter()
current_jewel = jewel_counter._get_jewel(gid, uid)  #获取当前宝石数        
jewel_counter._reduce_jewel(gid, uid, current_jewel) #减少num的宝石
score_counter._add_score(gid, uid, current_jewel)
msg=f'兑换成功！已将{current_jewel}宝石兑换成金币'
await bot.send(ev, msg)
```

# 猜头像

部分数据过时。按照需要，可以更改头部调用，如果贵族决斗安装了的话：
```python
from hoshino.modules.priconne.pcr_duel import _pcr_duel_data as _pcr_data
from hoshino.modules.priconne.pcr_duel import duel_chara as chara
```
但是会自动加入其他dlc。

只是更新pcr的话，可以改priconne下数据文件，手动添加新的角色。

# Daily_News

on_fullmatch 关键词过多超过2个，需要删除，因为nonebot过低

# HosBotManagerWeb

与shebot网页管理重复，删除shebot下webServiceManager

# 群聊输出二维码

windows server 2012r 出现msvcr120.dll文件丢失问题
[https://www.microsoft.com/zh-cn/download/confirmation.aspx?id=40784](https://www.microsoft.com/zh-cn/download/confirmation.aspx?id=40784)下载对应版本


# 需要更新插件

[https://github.com/zyujs/pcr_calendar](https://github.com/zyujs/pcr_calendarr)
[https://github.com/cc004/pcrjjc2](https://github.com/cc004/pcrjjc2)

