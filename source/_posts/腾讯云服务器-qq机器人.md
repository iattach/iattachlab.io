---
title: 腾讯win云服务器 + pcr机器人(hoshino_xcw),从头开始手动装全过程记录
author: iattach
avatar: 'https://wx1.sinaimg.cn/large/006bYVyvgy1ftand2qurdj303c03cdfv.jpg'
authorLink: hexo.iattach.top
categories: 技术
comments: true
copyright: false
date: 2021-12-22 10:27:05
authorAbout:
authorDesc:
tags:
keywords: 腾讯云服务器,pcr机器人,pcrbot,hoshino_xcw,hoshino,qq机器人，云服务器
description:
photos:
---

# 前言

因为手里有多个服务器，机器人某天挂了，手残把服务器换镜像了。。。有感而发，开始记录下过程供自己日后参考。

# 腾讯云服务器初始设置

服务器镜像最好选windows server 2012。windows是因为对小白，和咸鱼（指我）友好。2012是在windows8不内置defender，而2016和2019都是在win10基础上改的，都内置windows defender，开启之后加远程桌面，任务管理器显示内存占用达到50%。懒得去找2016/2019怎么关了，就先用2012吧。

服务器第一次或者重装镜像后，都需要重置密码，方便远程桌面登陆。

## 关闭异常登陆通知

因为异地登陆云服务器，结果发了一堆邮件。参考[https://cloud.tencent.com/document/product/296/60061](https://cloud.tencent.com/document/product/296/60061)，但是不可以关闭异常登录检测。

在[https://console.cloud.tencent.com/cwp/manage/loginLog](https://console.cloud.tencent.com/cwp/manage/loginLog)中，对登陆历史中自己的ip地址加入白名单。

## 端口开放

来源可以默认 0.0.0.0/0 对所有ip及其端口开放。

| 端口 | 协议 | 备注 |
| - | - | - |
| 21 | TCP | FTP用 |
| 22 | TCP | linux远程登录 |
| 80 | TCP | HTTP端口 |
| 443 | TCP | HTTPS用 |
| 3389 | TCP | windows远程登录 |
| 8080 | TCP | 用 |
| 9222 | TCP | MySQL用 |
| ALL | ICMP | 放通ping |

![Caption](/assets/wallpaper-2311325.jpg)

## 软件安装

所有的下载当然先从内置的IE11开始，打开后选**不使用推荐设置**。然后**Internet 选项**下的**安全**将允许级别调为中。使用**推荐设置**的话，这里是灰色不可调的，误选的话，在**安全**选项卡下面**自定义级别**下面拉到最下面将**文件下载**选择启用。

PS：传文件可以使用[https://transfer.sh/](https://transfer.sh/)，国内外访问都很快。远程登陆传文件实在太慢了。

### 浏览器
chrome内存泄漏，且因为国内问题无法下载。建议使用firefox或者edge，注意firefox国内版本和国际版账户不通用。

### python

python下载国内镜像：[https://npm.taobao.org/mirrors/python/](https://npm.taobao.org/mirrors/python/)，最新版本选择python-x.x.x-amd64.exe。

安装注意勾选 ADD Python x.x to PATH。

### Git

git for windows国内镜像：[https://npm.taobao.org/mirrors/git-for-windows/](https://npm.taobao.org/mirrors/git-for-windows/), 下载Git-x.xx.x-64-bit.exe  

pip安装国内源设置，参考：[https://blog.csdn.net/u011107575/article/details/109901086](https://blog.csdn.net/u011107575/article/details/109901086)

使用配置文件，一次解决

windows下，直接在 C:\Users\Administrator 目录中创建一个pip目录，再新建文件pip.ini，填入以下代码。（这里用的是清华的数据）
```bash
# pip.ini

[global]
index-url = https://pypi.tuna.tsinghua.edu.cn/simple #设置源地址

[install]
trusted-host = pypi.tuna.tsinghua.edu.cn #信任源地址
```
注意不要讲#及后面的东西加入，ini不能区分识别注释

### notepad++

代码文件轻量编辑器，暂时没发现国内源。虽然国内网站有但是会捆绑软件之类，有条件建议下载官方安装包（打开比较慢）：[https://notepad-plus-plus.org/downloads/](https://notepad-plus-plus.org/downloads/)

### ffmpeg 非必须

参考：[https://docs.go-cqhttp.org/guide/quick_start.html#%E6%9B%B4%E6%96%B0](https://docs.go-cqhttp.org/guide/quick_start.html#%E6%9B%B4%E6%96%B0)

为了支持任意格式的语音发送, 你需要安装 ffmpeg 。

从[https://www.gyan.dev/ffmpeg/builds/ffmpeg-release-full.7z](这里)下载并解压, 并为 bin 这个文件夹添加环境变量。

如果遇到下载速度缓慢的问题可以用[https://downloads.go-cqhttp.org/ffmpeg-release-full.7z](这个源)。

然后在 cmd 输入 (不能使用 powershell）
```bash
setx /M PATH "C:\Program Files\ffmpeg\bin;%PATH%"
```
自行将这个指令中的 C:\Program Files 替换成你的解压目录。


# 部署pcr qq机器人

机器人采用的是hoshino_xcw为基础，后续逐渐添加其他插件。以上软件部分及本部分参考：[https://github.com/pcrbot/hoshino_xcw/wiki/%E9%83%A8%E7%BD%B2%E6%B5%81%E7%A8%8B](https://github.com/pcrbot/hoshino_xcw/wiki/%E9%83%A8%E7%BD%B2%E6%B5%81%E7%A8%8B，主要参考文档) 和 [https://github.com/Soung2279/haru-bot-setup/](windows下的安装，副参考)，注意go-cqhttp,hoshino_xcw与yobot分别独立，可以单独安装互不干扰。

## go-cqhttp

这个软件是用来获取qq消息的，参考：[https://docs.go-cqhttp.org/guide/quick_start.html#%E6%9B%B4%E6%96%B0](https://docs.go-cqhttp.org/guide/quick_start.html#%E6%9B%B4%E6%96%B0)

从[https://github.com/Mrs4s/go-cqhttp/releases](https://github.com/Mrs4s/go-cqhttp/releases)下载放在单独的目录下，**go-cqhttp_windows_amd64.exe**，可以用github文件加速之类的工具加快下载速度。

启动先直接打开，配置文件选择**3：反向Websocket通信**，然后会自动生成配置文件。配置好后，再启动，选择手机扫码登陆。下面的几个命令可以用在exe同一目录下的bat脚本运行。注意exe文件名须一致。

### 配置文件config.yml + device.json

文件自动生成后，需要修改第一行qq号为机器人的qq号和最后的ws-reverse部分如下：

```yaml
servers:
  # 反向WS设置
  # 8080 为 HoshinoBot 默认端口号
  - ws-reverse:
      universal: ws://127.0.0.1:8080/ws/
      api: ""
      event: ""
      reconnect-interval: 3000
      middlewares:
        <<: *default # 引用默认中间件
  # 9222 为 yobot 默认端口号
  - ws-reverse:
      universal: ws://127.0.0.1:9222/ws/
      api: ""
      event: ""
      reconnect-interval: 3000
      middlewares:
        <<: *default # 引用默认中间件
```
来源：[https://cn.pcrbot.com/deploy-hoshinobot-on-centos/](https://cn.pcrbot.com/deploy-hoshinobot-on-centos/)

修改后再次运行go-cqhttp，为了玄学避免风控，可以更改device.json中protocol值0为1，然后再启动go-cqhttp，这样机器人qq头像一直显示为手机在线，但是这会和手机端登陆bot账号冲突，因为一个qq不能同时在两个手机端登陆。

启动后，应该会显示连接到反向ws服务器出现错误之类的，这是正常现象，因为yobot和qq机器人主体还未启动。这时候在机器人qq加入的qq群发送消息或者私聊，会显示在go-cqhttp运行的窗口内。

注意：这个json在第一次用go-cqhttp登陆qq后才会有。

### 加快启动速度命令+bat脚本
```bash
.\go-cqhttp.exe faststart
```

### 更新命令+bat脚本
```bash
.\go-cqhttp.exe update https://github.com.cnpmjs.org
```

## 安装HoshinoBot_xcw

右击想要安装的目录路径，右击打开git bash，输入
```bash
git clone https://github.com.cnpmjs.org/sanshanya/hoshino_xcw.git
```

### 安装依赖

先安装基本插件，在**XCW/装依赖/A号套餐**下,执行升级pip，装依赖1和装依赖2三个bat脚本。注意：python版本不是3.8的话，需要修改bat脚本，将**py**命令后面的`-3.8`选项删除。

为了使用所有插件，使用B号套餐。在**XCW/装依赖/B号套餐**下复制**自动装依赖.ps1**到**XCW/hoshino**下，并打开此文件，将**py**命令后面的`-3.8`选项删除（因为之前我们装的是3.9版本的python），再用powershell执行:

```bash
cd C:\........\XCW\Hoshino #机器人文件夹绝对路径

.\自动装依赖.ps1
```

### 修改配置文件

首先来到**XCW\Hoshino**

重要：将**__bot__示例.py**改名为**__bot__.py**并覆盖**XCW\Hoshino\hoshino\config**文件夹内的**__bot__.py**

使用notepad++编辑**__bot__.py**,根据注释进行修改.

首次使用hoshino可暂时只修改第13行,即修改最高权限用户

保存后关闭即可

推荐修改的配置文件部分如下:

```python
'''---拥有最高权限的用户们的QQ---'''
SUPERUSERS = [1234567895]    # 填写超级用户的QQ号，可填多个用半角逗号","隔开
PYS ={123214342}            #高级权限用户的QQ号
###################################
'''---------昵称及网址----------'''
NICKNAME = r'镜华|小仓唯|露娜|at,qq=124324234'       # 设置bot的昵称，at，qq=xxxxxxxx处为bot的QQ号,呼叫昵称等@bot
IP = '333.33.33.3'                                      #修改为你的服务器ip,推荐修改
public_address = '333.33.33.3:8080'                     #修改为你的服务器ip+端口,推荐修改
PassWord = '123456'                                           #登录一些只限维护人知道密码的网页
###################################
IMAGE_PATH = "C:\\go-cqhttp\\data\\images"                 #MiraiGO用这条,保持默认即可  PS：因为之前我们独立装了go-cqhttp，这行修改为go-cqhttp的绝对路径

# 资源库文件夹，需可读可写，windows下注意反斜杠转义
RES_DIR = "C:\\res" #res文件的绝对路径，配合插件xcw使用
```

其他插件安装及相关问题请见[]()

## 安装yobot

请于[https://github.com/yuudi/yobot/releases/tag/v3.6.14](https://github.com/yuudi/yobot/releases/tag/v3.6.14)下载yobot便携版：yobot-.RELEASE_VER.v.-windows64.zip，解压后放在单独的目录下。双击“yobot.exe”启动服务

以下部分内容来源参考：[https://yobot.win/install/Windows-gocqhttp/](https://yobot.win/install/Windows-gocqhttp/)

### 验证安装

向机器人发送“version”，机器人会回复当前版本

向机器人私聊发送“登录”，机器人会回复登录链接（第一个发送登录的人自动获得主人权限）

向机器人发送“重启”（需要权限），机器人会重启

### yobot网页设置

在私聊发送“登陆”后，打开网页设置密码。然后点击左上角返回，在设置项设置基本信息及boss血量。

截止2021-11：

| 阶段 | 起始周目数 | 一王 | 二王 | 三王 | 四王 | 五王 |
| - | - | - | - | - | - | - |
| 1阶段 | 1 | 6000000 | 8000000 | 10000000 | 12000000 | 15000000 |
| 2阶段 | 4 | 6000000 | 8000000 | 10000000 | 12000000 | 15000000 |
| 3阶段 | 11 | 7000000 | 9000000 | 12000000 | 14000000 | 17000000 |

