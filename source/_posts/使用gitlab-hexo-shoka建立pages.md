---
title: 使用gitlab + hexo shoka建立镜像网站pages
author: iattach
avatar: 'https://wx1.sinaimg.cn/large/006bYVyvgy1ftand2qurdj303c03cdfv.jpg'
authorLink: hexo.iattach.top
categories: 技术
comments: true
copyright: false
date: 2021-12-24 11:24:05
authorAbout:
authorDesc:
tags:
keywords: gitlab, hexo, shoka, pages
description:
photos:
---

在之前的文章[建站过程](https://hexo.iattach.top/2021/12/21/%E5%BB%BA%E7%AB%99%E8%BF%87%E7%A8%8B/)中有提到用github + hexo来建站，但是因为百度搜索爬虫不能直接爬github pages，本次采用gitlab建立镜像网站，这样就可以让百度能够搜索的到。

# DNS

首先跟之前一样github的dns记录一样，添加两条记录：域名网址和www域名网址，然后类型**CNAME**，线路类型百度，记录值填对应的gitlab pages的网址。这样百度爬虫会被导向gitlab上面。

# gitlab设置

来源：[https://www.jianshu.com/p/c42835a5a64f](https://www.jianshu.com/p/c42835a5a64f)

在Gitlab创建一个repository，名字为xxx.gitlab.io，xxx就是你的Gitlab用户名。

```bash
git init
git remote add origin git@gitlab.com:xxx/xxx.gitlab.io.git
```
注意：如果themes下的主题是git clone下来，那么会自带.git文件夹，推送到gitlab上时不会推送源文件，在git add前务必将使用themes的.git删掉。如果不小心add上了，请执行一下命令清理缓存再重新add文件：

```bash
git rm -r --cached .
```

然后再博客根目录下创建`.gitlab-ci.yml`,内容参考：[https://gitlab.com/pages/hexo/blob/master/.gitlab-ci.yml](https://gitlab.com/pages/hexo/blob/master/.gitlab-ci.yml)

```yaml
image: node: #最新版本

cache:
  paths:
    - node_modules/

before_script:
  - npm install hexo-cli -g
  - test -e package.json && npm install
  - export HEXO_ALGOLIA_INDEXING_KEY=xxx #xxx=你的algolia admin key
  - hexo algolia #如果装了algolia的话
  - hexo generate

pages:
  script:
    - hexo generate
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

git推送到远程仓库之后，GitLab CI 会自动开始运行，构建成功以后稍等几分钟，打开 https://xxx.gitlab.io 应该就能看到自己的博客了。

如果想隐藏博客源码，又不想影响博客的正常访问，可以在仓库的**Settings -> General -> Visibility, project features, permissions**里面将仓库权限`Project visibility`设置为**private**，然后把下面的**Pages**选项改为**Everyone**。

# Gitlab pages的个人域名https 搭配dnspod签发的证书

需要在pages的选项下，建自已的Pages Domain，然后再Certificate选项填写对应的密文：

- Certificate (PEM) ： 放入xxx_bundle.pem中的内容，所有都要填写。因为是通过中级 CA 机构颁发的证书，拿到的证书文件包含多份证书。

- Key (PEM) ： 放入xxx.key中的内容。

然后就可以打开https对应的链接了

# 附言：bash脚本

```bash
@echo on
cmd /c hexo clean
cmd /c hexo g
cmd /c set HEXO_ALGOLIA_INDEXING_KEY=xxx #xxx=你的algolia admin key
cmd /c hexo algolia
cmd /c hexo d
cmd /c git add .
cmd /c git commit -m "upload file %DATE%_%TIME%"
cmd /c git push
pause
```