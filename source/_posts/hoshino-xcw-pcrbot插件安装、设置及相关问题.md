---
title: hoshino_xcw pcrbot插件安装、设置及相关问题
author: iattach
avatar: 'https://wx1.sinaimg.cn/large/006bYVyvgy1ftand2qurdj303c03cdfv.jpg'
authorLink: hexo.iattach.top
categories: 坑
comments: true
copyright: false
date: 2021-12-24 16:00:35
authorAbout:
authorDesc:
tags: 插件,hoshino_xcw,pcrbot,qq机器人，群聊机器人，qq
description:
photos:
keywords: 
---

# 前言

因为插件安装有各种各样的问题和设置，所以单开一章来讲。

# 错误 aiochttp

按照之前文章设置完后，运行**run.py**。会显示如下错误：
```bash
Traceback (most recent call last):
  File "C:\XCW\Hoshino\run.py", line 1, in <module>
    import hoshino
  File "C:\XCW\Hoshino\hoshino\__init__.py", line 3, in <module>
    import aiocqhttp
ModuleNotFoundError: No module named 'aiocqhttp'
```
因为依赖没装，先去执行A号套餐。

# 错误 PIL
```bash
Traceback (most recent call last):
  File "C:\XCW\Hoshino\run.py", line 1, in <module>
    import hoshino
  File "C:\XCW\Hoshino\hoshino\__init__.py", line 54, in <module>
    from . import R
  File "C:\XCW\Hoshino\hoshino\R.py", line 6, in <module>
    from Pillow import Image
ModuleNotFoundError: No module named 'Pillow'
```
因为pil本地安装路径文件名为小写，需要改为大写。。。。。。
```plain
C:\Users\Administrator\AppData\Local\Programs\Python\Python39\Lib\site-packages\pil

改为

C:\Users\Administrator\AppData\Local\Programs\Python\Python39\Lib\site-packages\PIL
```
注意：如果其他插件用到，pip安装会覆盖此文件夹名，改为小写。所以谨慎重安装此包，否则每次需要将其改为大写。

# cv2 load 错误

这并不是pip安装的问题，是服务器windows本身设置的问题，当然需要先装opencv-python，然后选择“我的电脑”——“属性”——“管理”——”添加角色和功能“——勾选”桌面体验“，点击安装，安装之后重启即可。（windows server 2012）

参考：[https://blog.csdn.net/javastart/article/details/113306939](https://blog.csdn.net/javastart/article/details/113306939)

# 需要手动安装的包

保险起见，可以执行一下安装，杜绝插件出现的一些module未安装错误。
```bash
pip install fuzzywuzzy
pip install jieba
pip install opencv-python
pip install bs4
pip install pandas
pip install pinyin
pip install openpyxl
```
# 存在bug的插件

- B站动态 bilidynamicpush
- 官方漫画 priconne/comic.py (需要梯子)
- 台服新闻 (需要梯子)
- pcrjjc2_xcw 中 **sv_help**名字定义不统一，需要修改代码/频率太快，需要修改间隔时间
- setu_mix pivix (需要梯子)