---
title: win10 右键菜单第一次打开慢
author: iattach
avatar: 'https://wx1.sinaimg.cn/large/006bYVyvgy1ftand2qurdj303c03cdfv.jpg'
authorLink: hexo.iattach.top
categories: 坑
comments: true
copyright: false
date: 2021-12-23 01:29:02
authorAbout:
authorDesc:
tags:
keywords: win10,右键菜单，慢
description:
photos:
---

最近苦恼很长时间，右击图片或视频第一次都很慢，弄了很多右键菜单管理软件都无果。

结果突然想起来win10**事件查看器**应该有东西，一看果然，每次转圈慢都有这个事件：

```plain
服务器 {3C5E2B20-B911-44E2-A2DD-9F05E7B5E775} 没有在要求的超时时间内向 DCOM 注册。
```
在注册表查了下，找到了下面这个键值：

HKEY_CLASSES_ROOT\WOW6432Node\CLSID\{3C5E2B20-B911-44E2-A2DD-9F05E7B5E775}

看了下程序名字**PhotoViewerHelper Class**，在google搜了下什么都没有。

翻了翻下面的路径: HKEY_CLASSES_ROOT\WOW6432Node\CLSID\{3C5E2B20-B911-44E2-A2DD-9F05E7B5E775}\LocalServer32

一看，呦，这不是苹果的东西吗。

...........\Apple\Internet Services\ApplePhotoStreams.exe

结果把苹果东西全卸载（我是不经常用，经常用可以卸载重装），右键菜单就好了，不再转圈了，呵呵呵呵呵。
