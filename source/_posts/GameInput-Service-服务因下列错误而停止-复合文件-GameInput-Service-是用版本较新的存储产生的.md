---
title: 'GameInput Service 服务因下列错误而停止: 复合文件 GameInput Service 是用版本较新的存储产生的'
author: iattach
avatar: 'https://wx1.sinaimg.cn/large/006bYVyvgy1ftand2qurdj303c03cdfv.jpg'
authorLink: hexo.iattach.top
categories: 技术
comments: true
copyright: false
date: 2024-01-12 11:37:09
authorAbout:
authorDesc:
tags:
keywords:
description:
photos:
---

win11每天半夜重启死机，查了事件查看器，发现标题错误，解决方案引用reddit

The fix is simple. No need to wait for MS to address this issue.
解决方法很简单。无需等待 MS 解决此问题。

Double click both the Gaminput services and look at their location ("path to executable" it is called). You will see that one is located in your C:\Program Files (x86)\ folder, and the other is located in C:\Windows\System32 folder. With your explorer app of preference open both folders, find the executable (GameInputSvc.exe) and hover over it with your mouse. In a second a window will appear. Look at the creation date of the executable. I'm almost 100% certain that the one located in your Program Files (x86) is the the old one (creation date around beginning 2023)
双击两个 Gaminput 服务并查看它们的位置（称为“可执行文件的路径”）。您将看到一个位于 C：\Program Files （x86）\ 文件夹中，另一个位于 C：\Windows\System32 文件夹中。使用首选的资源管理器应用程序打开两个文件夹，找到可执行文件 （GameInputSvc.exe） 并用鼠标悬停在它上面。一秒钟后，将出现一个窗口。查看可执行文件的创建日期。我几乎 100% 确定位于您的程序文件 （x86） 中的那个是旧的（创建日期约为 2023 年初）

Then check your installed apps (start > settings > apps > installed apps). You will see Microsoft Game Input listed there. The date is somewhere around Feb/March 2023, so this one is old.
然后检查已安装的应用程序（开始>设置>应用程序>已安装的应用程序）。您将看到 Microsoft 游戏输入列在那里。日期在 2023 年 2 月/3 月左右，所以这个日期很旧。

Click uninstall !
点击卸载 ！

Then you will see that the executable in the Program Files (x86) is removed. If there are leftovers just manually remove them (delete the complete Microsoft Gameinput folder - you will probably get a warning so you must delete this as administrator)
然后，您将看到Program Files（x86）中的可执行文件已被删除。如果有剩余部分，只需手动删除它们（删除完整的 Microsoft Gameinput 文件夹 - 您可能会收到警告，因此您必须以管理员身份将其删除）

Restart your PC, check services and only 1 Gameinput will be listed there. If the service is not started, double click it, set it on automatic start, and finally start the service.
重新启动您的 PC，检查服务，那里只会列出 1 个 Gameinput。如果服务未启动，请双击它，将其设置为自动启动，最后启动服务。

The event viewer will no longer display the warnings.
事件查看器将不再显示警告。