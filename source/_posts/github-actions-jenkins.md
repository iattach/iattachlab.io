---
title: github actions jenkins
author: iattach
avatar: 'https://wx1.sinaimg.cn/large/006bYVyvgy1ftand2qurdj303c03cdfv.jpg'
authorLink: hexo.iattach.top
categories: 技术
comments: true
copyright: false
date: 2024-09-16 10:26:39
authorAbout:
authorDesc:
tags:
keywords:
description:
photos:
---

# github workflow 设置yaml文件

```yaml
name: Frontend pr workflow
on: # 在develop分支上触发PR的build
    pull_request:
    branches: ["develop"]

jobs:
    build: # 任务名
        runs-on: [self-hostsd]
        steps: # 步骤列表
        - uses: actions/checkout@v3 # 步骤使用actions组织的action，切换仓库，版本v3
        - name: Use Node.js 16.xx.x # 使用内部action
          uses: setupnoderepo
          with: # 此处为传参
            node-version: 16.xx.x
            registry-url: internal public repository url

        - name: cache npm dependencies # 使用actions组织的action，仓库缓存
          uses: actions/cache@v3
          id: npm_cache
          with:
            path: node_modules
            key: ${{ runner.os }}-node-${{ bashFiles('package-lock.json') }}
        
        - name: install packages # 运行bash命令的步骤
          run: npm install
          if: steps.npm_cache.outputs.cache-hit != 'true' #只在没有缓存成功的状态下运行

        - run: npm run build # 运行bash命令

        - run: npm run test

        - name: install and start sonar analysis # 使用内部action 仓库
          uses: xxx/sonar-xx@stable
          with:
            scanner-version: 'xxx'
            sonar-properties: 'sonar-project.properties'
            sonar-token: ${{ secrets.SONAR_TOKEN}} # token位于仓库的设置安全选项处
        #自定义action 
        
        - name：my custom private github actions
          uses: ./.github/actions/frontend-ci # 这里不能使用 Org/自己的仓库@版本 只能使用相对路径
          with:
            docker-username: ${{test}}
            docker-password: ${{test}}
            sonar-dsp-token: ${{test}}
            build-image: false
```