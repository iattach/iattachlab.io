---
title: Game day 202409
author: iattach
avatar: 'https://wx1.sinaimg.cn/large/006bYVyvgy1ftand2qurdj303c03cdfv.jpg'
authorLink: hexo.iattach.top
categories: 技术
comments: true
copyright: false
date: 2024-09-02 11:16:46
authorAbout:
authorDesc:
tags:
keywords:
description:
photos:
---

# 错误 can not pull image

检查mirantis secure registry，tags为空，重新run pipeline来重新部署image

# log 检查到数据库链接失败

首先检查database url是否正确，之后检查本地连接，发现无法连接。之后上do it know查看是否database正常，检查到database被关闭。action重启数据库

# liveness check failed

config中有个service占用了application使用的port，导致application重启失败，更改端口到8080

# 身份验证失败

uat环境不能登录，检查homologation设置，发现k8s设置中有两项中不同，检查并纠正


