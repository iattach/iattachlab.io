---
title: QQ图片文件夹Group2清理
author: iattach
avatar: 'https://wx1.sinaimg.cn/large/006bYVyvgy1ftand2qurdj303c03cdfv.jpg'
authorLink: hexo.iattach.top
categories: 技术
comments: true
copyright: false
date: 2021-12-26 18:13:01
authorAbout:
authorDesc:
tags:
keywords: QQ,Group2，文件清理
description:
photos:
---
参考：[https://bbs.saraba1st.com/2b/thread-2009182-1-1.html](https://bbs.saraba1st.com/2b/thread-2009182-1-1.html)

QQ的自带清理工具无法扫描到Group2文件夹，QQ Group2整个文件夹超级大打开都要半天. 但是如果只是全清空的话包括近期的所有群图片就都没了。

但是tim自带的清理工具又扫不到这个文件夹，最后通过在文件夹里面建立一个链接解决了。

在管理员权限的cmd里进入Image文件夹，然后运行
```bash
mklink /d Group  .\Group2
```
建立一个Group 到Group2的链接，之后再打开自带的文件清理器就能扫到这个文件夹了。
