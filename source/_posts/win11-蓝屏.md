---
title: win11-蓝屏
author: iattach
avatar: 'https://wx1.sinaimg.cn/large/006bYVyvgy1ftand2qurdj303c03cdfv.jpg'
authorLink: hexo.iattach.top
categories: 技术
comments: true
copyright: false
date: 2023-09-26 19:37:48
authorAbout:
authorDesc:
tags:
keywords:
description:
photos:
---

疯了，这几天开机疯狂蓝屏记录一下怎么解决

# 检查系统日志

打开系统工具 - 事件查看器 - windows 日志 - 系统

然后右侧筛选当前日志，事件级别选择关键、警告和错误。

# 10016事件

典型描述如下：
```
应用程序-特定 权限设置并未向在应用程序容器 不可用 SID (不可用)中运行的地址 LocalHost (使用 LRPC) 中的用户 DESKTOP-SF1U8RI\machilus SID (S-1-5-21-1608297252-3045228917-1571507988-1001)授予针对 CLSID 为 {2593F8B9-4EAF-457C-B68A-50F6B8EA6B54}、APPID 为 {15C20B67-12E7-4BB6-92BB-7AFF07997402} 的 COM 服务器应用程序的 本地 激活 权限。此安全权限可以使用组件服务管理工具进行修改。
```
| CLSID                                | APPID                                | DCOM                | CLSID权限/APPID权限 | DCOM更改 |
|--------------------------------------|--------------------------------------|---------------------|---------------------|----------|
| 2593F8B9-4EAF-457C-B68A-50F6B8EA6B54 | 15C20B67-12E7-4BB6-92BB-7AFF07997402 | PerAppRuntimeBroker | 已更改/已更改       | 已更改   |
| 7022A3B3-D004-4F52-AF11-E9E987FEE25F | ADA41B3C-C6FD-4A08-8CC1-D6EFDE67BE7D | WPN SRUM COM Server | 已更改/已更改       | 已更改   |
| 6B3B8D23-FA8D-40B9-8DBD-B950333E2C52 | 4839DDB7-58C2-48F5-8283-E1D1807D0D7D | ShellServiceHost    | 已更改/已更改       | 已更改   |
| 21B896BF-008D-4D01-A27B-26061B960DD7 | 03E09F3B-DCE4-44FE-A9CF-82D050827E1C | 无                  | 已更改/已更改       | 已更改   |
| 8CFC164F-4BE5-4FDD-94E9-E2AF73ED4A19 |                                      |                     |                     |          |

# Windows.SecurityCenter.SecurityAppBroker

```
1. Open Registry Editor. To do that:

1. Simultaneously press the Win image + R keys to open the run command box.
2. Type regedit and press Enter to open Registry Editor.

regedit
2. In Registry, navigate to the following location:

HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\wscsvc
3. At the right pane open the DelayedAutoStart REG_DWORD value.

Disable DelayedAutoStart

4. Change the value data from 1 to 0 and click OK.
```

# 错误

服务器 {8CFC164F-4BE5-4FDD-94E9-E2AF73ED4A19} 没有在要求的超时时间内向 DCOM 注册。

未找到解决办法，组策略关闭失败，跟webexperience小组件有关。