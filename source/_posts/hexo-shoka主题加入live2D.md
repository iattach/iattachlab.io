---
title: hexo - shoka主题加入live2D
author: iattach
avatar: 'https://wx1.sinaimg.cn/large/006bYVyvgy1ftand2qurdj303c03cdfv.jpg'
authorLink: hexo.iattach.top
categories: 技术
comments: true
copyright: false
date: 2021-12-25 23:31:05
authorAbout:
authorDesc:
tags:
keywords: hexo,shoka,live2D
description:
photos:
---

# 简单设置

在`themes\shoka\layout\_partials`目录下创建`live2d.njk`文件，放入以下代码：

```html
<script src="https://cdn.jsdelivr.net/gh/stevenjoezhang/live2d-widget/autoload.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery/dist/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome/css/font-awesome.min.css">
```
然后再同目录下的`layout.njk`下在`</body>`前加入
`{{ partial('_partials/live2d.njk') }}`

参考：[https://github.com/stevenjoezhang/live2d-widget](https://github.com/stevenjoezhang/live2d-widget)

# 复杂自定义设置

## 方式1：基于简单设置上的自定义

参考: [https://pyherman.github.io/2019/08/10/hexo%E5%A6%82%E4%BD%95%E5%8A%A0%E5%85%A5%E8%87%AA%E5%AE%9A%E4%B9%89live2d%E5%90%89%E7%A5%A5%E7%89%A9/](https://pyherman.github.io/2019/08/10/hexo%E5%A6%82%E4%BD%95%E5%8A%A0%E5%85%A5%E8%87%AA%E5%AE%9A%E4%B9%89live2d%E5%90%89%E7%A5%A5%E7%89%A9/)

### 本地文件
在博客源文件（即 source）目录下，执行前述的`git clone`命令。重新部署博客时，相关文件就会自动上传到对应的路径下。为了避免这些文件被Hexo插件错误地修改，可能需要设置`skip_render`。
```bash
git clone https://github.com/stevenjoezhang/live2d-widget.git
```
然后在`_config.yml`中加入git clone后文件路径，防止被二次修改：
```yaml
skip_render: live2d-widget/*
```
修改之前简单设置中`autoload.js`中的常量`live2d_path`为`live2d-widget`这一目录的路径：
```js
const live2d_path = "/live2d-widget/";
```

### CDN

直接修改之前简单设置中`autoload.js`中的常量`live2d_path`为`live2d-widget`这一目录CDN的URL。

## 方式2：插件安装

上面简单配置添加过的话，注意删除`layout.njk`添加的语句，避免重复冲突。

参考：[https://blog.imlete.cn/article/Live2d-Config.html](https://blog.imlete.cn/article/Live2d-Config.html) 和 [https://github.com/EYHN/hexo-helper-live2d](和https://github.com/EYHN/hexo-helper-live2d)

模型适配较少,但因为hexo有对应插件整合设置比较方便，执行以下命令：

```bash
npm install --save hexo-helper-live2d
```
然后再`_config.yml`中添加：
```yaml
live2d:
  enable: true # 是否开启live2d
  scriptFrom: local ## 脚本从本地引入
  pluginRootPath: live2dw/ # 模型根目录(指hexo g后生成public\live2dw文件名)
  pluginJsPath: lib/ # 依赖js的文件夹名(public\live2dw\lib)
  pluginModelPath: assets/ # 模型存放目录(public\live2dw\assets)
  tagMode: false # 标记模式(未知) 
  log: false # 日志
  model: # 模型
    use: live2d-widget-model-wanko # 使用的模型名称
  display: # 显示
    position: right # 显示在右边(left显示在左边)
    width: 150 # 宽度
    height: 300 # 高度
  mobile:
    show: true # 手机端是否显示
  react:
    opacity: 0.7 # 透明度
```