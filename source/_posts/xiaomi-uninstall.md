---
title: xiaomi-uninstall
author: iattach
avatar: 'https://wx1.sinaimg.cn/large/006bYVyvgy1ftand2qurdj303c03cdfv.jpg'
authorLink: hexo.iattach.top
categories: 技术
comments: true
copyright: false
date: 2023-10-05 11:46:24
authorAbout:
authorDesc:
tags:
keywords:
description:
photos:
---

小米5s垃圾中的垃圾

# adb卸载应用bat脚本

```bash
@echo on
cmd /c adb shell pm list packages
cmd /c adb devices
#adb shell pm uninstall --user 0 com.sohu.inputmethod.sogou.xiaomi #小米定制版搜狗输入法（用第三方应用替换Gboard等）
#adb shell pm uninstall --user 0 com.android.browser #浏览器（用第三方应用替换chrome等）
cmd /c adb shell pm uninstall --user 0 com.miui.video #小米视频 （用第三方应用替换nplayer等）
cmd /c adb shell pm uninstall --user 0 com.miui.gallery #相册（用第三方应用替换#Google相册等）
cmd /c adb shell pm uninstall --user 0 com.miui.player #音乐（用第三方应用替换）
cmd /c adb shell pm uninstall --user 0 com.miui.mishare.connectivity #小米互传（用第三方应用替换#vivo互传
#可卸载如：智能助理 搜索 小米黄页 快应用服务框架 服务与反馈 传送门 小爱翻译 小爱通话 小爱语音 游戏服务 analytics 用户反馈
cmd /c adb shell pm uninstall --user 0 com.miui.personalassistant #智能助理（负一屏）（直接卸载，自己不使用）
cmd /c adb shell pm uninstall --user 0 com.android.quicksearchbox #搜索（直接卸载，自己不使用）
cmd /c adb shell pm uninstall --user 0 com.miui.yellowpage #生活黄页(小米黄页)（直接卸载，自己不使用）
cmd /c adb shell pm uninstall --user 0 com.miui.hybrid #快应用服务框架(小米社区)（直接卸载，自己不使用） 
cmd /c adb shell pm uninstall --user 0 com.miui.miservice #服务与反馈（直接卸载，自己不使用）
cmd /c adb shell pm uninstall --user 0 com.miui.contentextension #传送门
cmd /c adb shell pm uninstall --user 0 com.xiaomi.aiasst.service #AI通话（小爱通话）
cmd /c adb shell pm uninstall --user 0 com.miui.voiceassist #小爱同学（小爱语音）（直接卸载，自己不使用） 
cmd /c adb shell pm uninstall --user 0 com.xiaomi.aiasst.vision #小爱翻译
cmd /c adb shell pm uninstall --user 0 com.xiaomi.gamecenter.sdk.service #游戏服务
cmd /c adb shell pm uninstall --user 0 com.miui.analytics #小米广告分析，必删（重启会自动安装）
cmd /c adb shell pm uninstall --user 0 com.miui.bugreport #用户反馈
# 2.2.2.其他可删除（欢迎留言补充）

cmd /c adb shell pm uninstall --user 0 com.miui.systemAdSolution #小米系统广告解决方案，必删
cmd /c adb shell pm uninstall --user 0 com.miui.translation.kingsoft #金山翻译
cmd /c adb shell pm uninstall --user 0 com.miui.translation.youdao #有道翻译
cmd /c adb shell pm uninstall --user 0 com.miui.translation.xmcloud #小米云翻译
cmd /c adb shell pm uninstall --user 0 com.miui.translationservice #翻译服务
cmd /c adb shell pm uninstall --user 0 com.xiaomi.ab #小米商城系统组件
cmd /c adb shell pm uninstall --user 0 com.miui.accessibility #小米闻声
cmd /c adb shell pm uninstall --user 0 com.xiaomi.migameservice #游戏高能时刻
cmd /c adb shell pm uninstall --user 0 com.miui.voicetrigger #语音唤醒
cmd /c adb shell pm uninstall --user 0 com.miui.nextpay #小米支付
cmd /c adb shell pm uninstall --user 0 com.xiaomi.payment #米币支付
cmd /c adb shell pm uninstall --user 0 com.miui.securityadd # 游戏加速
cmd /c adb shell pm uninstall --user 0 com.miui.hybrid.accessory # 智慧生活
cmd /c adb shell pm uninstall --user 0 com.xiaomi.macro #自动连招
cmd /c adb shell pm uninstall --user 0 com.miui.freeform # 自由窗口
cmd /c adb shell pm uninstall --user 0 com.miui.carlink #CarWith
cmd /c adb shell pm uninstall --user 0 com.xiaomi.vipaccount #小米社区
cmd /c adb shell pm uninstall --user 0 com.duokan.reader #阅读
cmd /c adb shell pm uninstall --user 0 com.xiaomi.gamecenter #游戏中心
# adb shell pm uninstall --user 0 com.miui.cit # CIT手机测试
#adb shell pm uninstall --user 0 com.miui.contentcatcher # 应用程序扩展服务
#adb shell pm uninstall --user 0 com.miui.maintenancemode # 维修模式
cmd /c adb shell pm uninstall --user 0 com.miui.securityadd # 游戏加速
#adb shell pm uninstall --user 0 com.miui.touchassistant # 悬浮球
#adb shell pm uninstall --user 0 com.miui.tsmclient # 小米智能卡
#adb shell pm uninstall --user 0 com.miui.phrase # 常用语
#adb shell pm uninstall --user 0 com.mipay.wallet # 小米钱包
#adb shell pm uninstall --user 0 com.xiaomi.joyose # 运动计步
#adb shell pm uninstall --user 0 com.xiaomi.mirror # MIUI+ Beta版
#adb shell pm uninstall --user 0 com.xiaomi.mircs # RCS 增强短信
#adb shell pm uninstall --user 0 com.xiaomi.otrpbroker # OTRP 协议协商程序（物联网）
#adb shell pm uninstall --user 0 com.miui.micloudsync # 小米云同步
#adb shell pm uninstall --user 0 com.miui.cloudservice.sysbase # 小米云服务系统基础
pause
```