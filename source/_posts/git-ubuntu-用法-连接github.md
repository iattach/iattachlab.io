---
title: git ubuntu 用法 连接github
date: 2021-03-23 15:50:21
author: iattach
avatar: avatar.jpg
authorLink: hexo.iattach.top
categories: 技术
tags:
keywords: ubuntu,git,github
---
# Git 初始 设置

sudo apt install git

 这里的“。。。。 ” 可以替换成自己的用户名

git config --global user.name "。。。。"

 这里的邮箱  。。。  替换成自己的邮箱

git config --global user.email  " 。。。 "

# 检查ssh key

cd ~/.ssh

ls

列出该文件下的文件，看是否存在 id_isa 和 id_isa.pub 文件（也可以是别的文件名，只要 yourName 和 yourName.pub 承兑存在），如果存在的话，证明已经存在 ssh key了，可以直接跳过 生成密钥 这一步骤

# 生成密钥

 这里的邮箱  。。。  替换成自己的邮箱

ssh-keygen -t rsa -C "。。。"

连续3个回车。如果不需要密码的话。
最后得到了两个文件：id_rsa和id_rsa.pub。

# 添加密钥到ssh-agent

# start the ssh-agent in the background
eval "$(ssh-agent -s)"

ssh-add ~/.ssh/id_rsa

最后吧id_rsa.pub的内容放到github上就行了

