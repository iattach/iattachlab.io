---
title: 装 hexo + shoka主题 遇到的小坑
author: iattach
avatar: avatar.jpg
authorLink: hexo.iattach.top
categories: 坑
comments: true
copyright: false
date: 2021-12-21 23:49:14
authorAbout:
authorDesc:
tags:
keywords: 百度，hexo，algolia，博客，建站
description:
photos:
---

# algolia搜索

新手：algolia装了没有用，即使填了key之后。

插件没装,参考 https://cyfeng.science/2020/04/11/hexo-next-algolia/

```bash
npm install --save hexo-algolia
#之后执行命令
hexo algolia
```

但是会报以下的错误
```plain
Please set an HEXO_ALGOLIA_INDEXING_KEY environment 
```

因为执行命令当前环境变量没设置
```plain
set  HEXO_ALGOLIA_INDEXING_KEY=你的Admin API Key #windows下的命令

export  HEXO_ALGOLIA_INDEXING_KEY=你的Admin API Key #linux下的命令
```

# lean cloud 评论
一开始设置的都对的，但就是发不了评论。查了下因为是国际版lean cloud，没设置主题中valine的serverURLs。这里应设置为官网给的REST API 服务器地址。

# hexo
hexo new 出来的md格式不知道在哪，默认文件格式是scaffolds/post.md决定的。scaffolds是用来存放模板的文件夹。

## 偷懒bat脚本
想偷点懒直接一键发布，就写了个bash脚本。然后又踩坑了。。。

起脚本名字为hexo.bat, 结果.bat脚本不断重复执行同一命令。因为与系统hexo这个命令冲突，所以需要起个不冲突的名字。

想要显示命令的执行结果，有的人说加了pause就行，有人说加了cmd /f k就行。其实都没用，需要写cmd /c "命令"。这样最后还能显示任意键退出，大为震惊。

注意：algolia应在generate之后，因为algolia需要generate产生的文件


```bash
@echo on
cmd /c "hexo clean"
cmd /c "hexo g"
cmd /c "set  HEXO_ALGOLIA_INDEXING_KEY=你的Admin API Key" #windows下的命令
cmd /c "hexo algolia"
cmd /c "hexo d"
pause
```
## 百度推送

之前的插件有装，后来忘了。参考：[https://soyl.tech/Hexo/baidu_url_submit.html](https://soyl.tech/Hexo/baidu_url_submit.html)

### 安装插件
```bash
npm install hexo-baidu-url-submit –save
```
在根目录_config.yml文件里加入以下代码：
```yaml
baidu_url_submit:
  count: 100 ## 提交最新的一个链接
  host: soyl.tech ## 在百度站长平台中注册的域名
  token: ******* ## 请注意这是您的秘钥， 所以请不要把博客源代码发布在公众仓库里!
  path: baidu_urls.txt ## 文本文档的地址， 新链接会保存在此文本文档里
```
其次，记得查看_config.ym文件中url的值， 必须包含是百度站长平台注册的域名。

### 加入新的deployer
```yaml
deploy:
- type: git ## 这是我原来的deployer
  repository: git@github.com:HeySoyl/HeySoyl.github.io.git
  branch: master
- type: baidu_url_submitter ## 这是新加的
```

### 执行 hexo d
```bash
INFO  Deploy done: git
INFO  Deploying: baidu_url_submitter
INFO  Submitting urls 
https://soyl.tech/uncategorized/yonghuxieyi.html
https://soyl.tech/uncategorized/cloudkit_question.html
{"remain":2961,"success":39}
INFO  Deploy done: baidu_url_submitter
```
看到”success”:39 表示提交成功了39条。

# 题外话：域名

弄了一下午服务器的域名，一开始是ping域名不通，结果是服务器的端口没放行。

后来ping是ping通了，但就是打不开域名，ip地址能打开。结果是因为换ip的缘故，没ssl证书或者之前的证书不匹配，再加上chrome等浏览器自动将http跳转为https，搞得我头大了一下午。

最后在chrome://net-internals/#hsts把以前的ssl删除了，并且地址换为http打头才ok。然后实在受不了，又重弄了个ssl证书，把https链路弄完整了，囧。。。。

吐槽：为什么域名绑定了github pages，子域名也要跟着绑一起了啊啊啊啊啊啊啊啊啊啊啊啊啊(dns a类型设了也没用 T T ）