---
title: Windows 11尝试安装黑苹果
author: iattach
avatar: 'https://wx1.sinaimg.cn/large/006bYVyvgy1ftand2qurdj303c03cdfv.jpg'
authorLink: hexo.iattach.top
categories: 技术
comments: true
copyright: false
date: 2023-08-27 20:22:11
authorAbout:
authorDesc:
tags:
keywords:
description:
photos:
---

因为没有第二台ios就尝试黑苹果定期登录一下苹果小号吧。

# VMWare安装

版本选择VMWare 16 pro，因为官网下载需要登录账号，建议搜索下官网下载直链。直接安装的版本没有macos选项，需要搜索unlocker vmware找到github的depot，但貌似项目已经存档了。。。。但还是可以用的。

# Macos安装

第一次建立好文件后打开系统直接蓝屏，因为amd的cpu？？搜了一下发现要加参数，在自动建立的vmware镜像文件中找到vmx后缀的文件并在尾部添加以下参数：
```bash
smc.version = "0"
cpuid.0.eax = "0000:0000:0000:0000:0000:0000:0000:1011"
cpuid.0.ebx = "0111:0101:0110:1110:0110:0101:0100:0111"
cpuid.0.ecx = "0110:1100:0110:0101:0111:0100:0110:1110"
cpuid.0.edx = "0100:1001:0110:0101:0110:1110:0110:1001"
cpuid.1.eax = "0000:0000:0000:0001:0000:0110:0111:0001"
cpuid.1.ebx = "0000:0010:0000:0001:0000:1000:0000:0000"
cpuid.1.ecx = "1000:0010:1001:1000:0010:0010:0000:0011"
cpuid.1.edx = "0000:0111:1000:1011:1111:1011:1111:1111"
smbios.reflectHost = "TRUE"
hw.model = "MacBookPro14,3"
board-id = "Mac-551B86E5744E2388"
keyboard.vusb.enable = "TRUE"
mouse.vusb.enable = "TRUE"
```

第二次蓝屏。。。。。
搜索vmware amd蓝屏解决得到以下方案，在windows11的控制面板中找到启用或关闭windows功能，打勾**适用于Linux的Windows子系统**和**虚拟机平台**两个选项。

这次倒是不蓝屏了，但是显示**未能启动虚拟机**，找寻所有方式无果，于是换了最新的vmware版本17pro和macos monterey镜像，加了上面的代码后结果就可以了！！！！！

WTFFFFFFFFFF

# macos安装

先进入**磁盘工具**把未格式化的磁盘**抹掉**，然后返回主界面安装os