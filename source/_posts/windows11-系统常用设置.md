---
title: windows11 系统常用设置
author: iattach
avatar: 'https://wx1.sinaimg.cn/large/006bYVyvgy1ftand2qurdj303c03cdfv.jpg'
authorLink: hexo.iattach.top
categories: 技术
comments: true
copyright: false
date: 2022-07-16 06:59:50
authorAbout:
authorDesc:
tags:
keywords:
description:
photos:
---

# 右键菜单

右键菜单有半秒延迟打开缓慢，参考[https://zhuanlan.zhihu.com/p/427268135](https://zhuanlan.zhihu.com/p/427268135)


## 使用注册表修改

首先，通过修改注册表，我们就可以将Win11的右键菜单改为老样式。下面是具体的方法。

运行“regedit”，开启注册表编辑器，定位到“HKEY_CURRENT_USER\SOFTWARE\CLASSES\CLSID”；

接着，右键点击“CLSID”键值，新建一个名为{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}的项；

右键点击新创建的项，新建一个名为InprocServer32的项，按下回车键保存；

最后选择新创建的项，然后双击右侧窗格中的默认条目，什么内容都不需要输入，按下回车键。

保存注册表后，重启explorer.exe，即可看到右键菜单恢复成旧样式了。

如果想要恢复成为Win11的设计，那么删掉InprocServer32的项就可以了。


# 换电脑步骤

## 旧电脑
* listray导出
* onenote导出

## 新电脑
* 装chrome
* 装驱动 *
* 激活win11 pro
* 移动所有windows个人文件夹
* listary导入
* 登录google账户
* potplayer
* 更改屏幕分辨率
* win11任务栏文件夹位置 C:\Users\machilus\AppData\Roaming\Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar
* 安装office
* 添加邮件mail
* 截屏使用多窗口
* 执行右键菜单 exe

### 技术栈安装
* git
* vscode
* nodejs
* ssh生成秘钥，github添加pub秘钥

### 代码执行
```bash
npm install -g hexo-cli
```
