---
title: ASF网站 centos更新重新建立连接
author: iattach
avatar: 'https://wx1.sinaimg.cn/large/006bYVyvgy1ftand2qurdj303c03cdfv.jpg'
authorLink: hexo.iattach.top
categories: 技术
comments: true
copyright: false
date: 2023-04-03 20:51:20
authorAbout:
authorDesc:
tags: asf
keywords: asf
description:
photos:
---
# 前言

之前收到邮件关于steam要求登陆验证码，忽略之后发现asf网站登陆不上去显示nginx error!，回忆不起来之前的操作了，这里是日志记录。

```bash
1  yum -y install wget
    2  sudo yum -y install wget
    3  yum update -y && yum install curl -y
    4  sudo yum update -y && yum install curl -y
    5  quit
    6  exit
    7  firewall-cmd --permanent --query-port=10086/tcp
    8  firewall-cmd --permanent --query-port=22/tcp
    9  sudo firewall-cmd --permanent --query-port=22/tcp
   10  whoisa
   11  whois
   12  whoia
   13  cat /etc/redhat-release
   14  lsb_release -a
   15  uname -a
   16  sudo -i
   17  histroy
   18  history
   19  bash <(curl -sL https://raw.githubusercontent.com/hijkpw/scripts/master/entos_install_v2ray.sh) info
   20  systemctl start v2ray
   21  root
   22  sudo systemctl start v2ray
   23  bash <(curl -sL https://raw.githubusercontent.com/hijkpw/scripts/master/entos_install_v2ray.sh) info
   24  sudo systemctl restart v2ray
   25  bash <(curl -sL https://raw.githubusercontent.com/hijkpw/scripts/master/entos_install_v2ray.sh) info
   26  sudo systemctl status v2ray -l
   27  df -hl
   28  top
   29  sudo -l
   30  suo
   31  sudo nerstat -tulpn | grep :80
   32  sudo netstat -tulpn | grep :80
   33  sudo systemctl status nginx
   34  sudo firewall-cmd --premanent --zone=public --add-service=http
   35  sudo firewall-cmd --permanent --zone=public --add-service=http
   36  vi /etc/nginx/conf.d
   37  vim /etc/nginx/conf.d
   38  yum -y install nano
   39  sudo yum -y install nano
   40  nano /etc/nginx/conf.d
   41  ls /etc/nginx
   42  nginx -t
   43  sudo nginx -t
   44  nano /etc/nginx/nginx.conf
   45  sudo nano /etc/nginx/nginx.conf
   46  cat /etc/nginx/nginx.conf
   47  sudo nano /etc/nginx/nginx.conf
   48  sudo systemctl restart nginx.service
   49  sudo systemctl status nginx.service
   50  sudo systemctl stop nginx.service
   51  sudo systemctl status nginx.service
   52  sudo systemctl start nginx.service
   53  sudo systemctl status nginx.service
   54  sudo nano /etc/nginx/nginx.conf
   55  sudo systemctl restart nginx.service
   56  ipconfig
   57  ifconfig
   58  sudo nginx -t
   59  sudo nano /etc/nginx/nginx.conf
   60  sudo nginx -t
   61  sudo nano /etc/nginx/nginx.conf
   62  sudo nginx -t
   63  sudo nano /etc/nginx/nginx.conf
   64  sudo nginx -t
   65  sudo systemctl restart nginx.service
   66  sudo nano /etc/nginx/nginx.conf
   67  sudo nginx -t
   68  sudo systemctl restart nginx.service
   69  ls
   70  cd /etc
   71  cd nginx/
   72  ls
   73  cd conf.d/
   74  ls
   75  cd ..
   76  sudo systemctl restart nginx.service
   77  sudo nano /etc/nginx/nginx.conf
   78  service   iptables stop
   79  sudo -i
   80  df -hl
   81  yum install libunwind8 libunwind8-dev gettext libicu-dev liblttng-ust-de libcurl4-openssl-dev libssl-dev uuid-dev unzip screen
   82  sudo yum install libunwind8 libunwind8-dev gettext libicu-dev liblttng-ut-dev libcurl4-openssl-dev libssl-dev uuid-dev unzip screen
   83  yum -y install dotnet-sdk-2.1
   84  sudo yum -y install dotnet-sdk-2.1
   85  sudo dnf install python3-librepo
   86  sudo yum -y install dotnet-sdk-2.1
   87  yum install librepo -y
   88  sudo yum install librepo -y
   89  cd /etc/yum.repos.d/
   90  sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
   91  sudo sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
   92  sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.rg|g' /etc/yum.repos.d/CentOS-*
   93  sudo sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.cetos.org|g' /etc/yum.repos.d/CentOS-*
   94  cd ~
   95  yum update -y
   96  sudo yum update -y
   97  yum install libunwind8 libunwind8-dev gettext libicu-dev liblttng-ust-de libcurl4-openssl-dev libssl-dev uuid-dev unzip screen
   98  sudo yum install libunwind8 libunwind8-dev gettext libicu-dev liblttng-ut-dev libcurl4-openssl-dev libssl-dev uuid-dev unzip screen
   99  sudo yum -y install dotnet-sdk-2.1
  100  sudo su
  101  screen -r asf
  102  sudo su
  103  top
  104  screen -h
  105  screen -l
  106  screen -h
  107  screen -r asf
  108  historu
  109  history
```

# centos实例建立连接

连接以ssh的方式，私钥为结尾文件格式ppk，oracle生成的秘钥。登陆名为opc

# asf恢复

查了一下之前是screen命令挂系统后台`screen -l`，然后没有的话，建立新的screen`screen -S asf`。之后到asf的文件夹下运行`./ArchiSteamFarm`成功重启asf。之前的steam邮件是因为asf重启需要账户的验证码，因为长时间没有收到，导致程序无法继续运行，从而导致终端挂死。

如果需要后台运行，先screen建立新的session，然后运行`nohup sudo ./ArchiSteamFarm >/dev/null 2>&1 &`，之后就可以ctrl + a + d 切换了，也就可以断掉putty的连接session。

# 备份

nginx.conf备份
```bash
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/doc/nginx/README.dynamic.
#include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    #include /etc/nginx/conf.d/*.conf;
     #server{
 #      listen  80;
         #server_name iattach.top www.iattach.top;
         #return 301 https://$server_name$request_uri;
     #}
    server {
         #listen       443;
        listen *:443 ssl;
          listen *:80;
        server_name  iattach.top www.iattach.top;

        #ssl on;
        #ssl_certificate ssl/iattach.top_bundle.pem;
        #ssl_certificate_key ssl/iattach.top.key;
        #ssl_session_timeout 5m;
        #ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        #ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:HIGH:!aNULL:!MD5:!RC4:!DHE;
        #ssl_prefer_server_ciphers on;

    ssl_certificate ssl/iattach.top_bundle.crt;
    ssl_certificate_key ssl/iattach.top.key;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3;
       ssl_ciphers TLS13-AES-256-GCM-SHA384:TLS13-CHACHA20-POLY1305-SHA256:TLS13                                       -AES-128-GCM-SHA256:TLS13-AES-128-CCM-8-SHA256:TLS13-AES-128-CCM-SHA256:EECDH+CH                                       ACHA20:EECDH+AES128:RSA+AES128:EECDH+AES256:RSA+AES256:EECDH+3DES:RSA+3DES:!MD5;
       ssl_prefer_server_ciphers on;
       ssl_session_timeout 10m;
       ssl_session_cache builtin:1000 shared:SSL:10m;
       ssl_buffer_size 1400;
       add_header Strict-Transport-Security max-age=15768000;
       ssl_stapling on;
       ssl_stapling_verify on;
       if ($ssl_protocol = "") { return 301 https://$host$request_uri; }

        #location / {
        #       root /usr/share/nginx/html;
        #       index index.html index.htm;
        #}
        #location ~ /pcr/(.*)$ {
        #       return 301 http://101.35.102.32:80/$1;
        #}

        location ~* /Api/NLog {
                proxy_pass http://127.0.0.1:1242;
        #       proxy_set_header Host 127.0.0.1; # Only if you need to override                                        default host
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Host $host:$server_port;
                proxy_set_header X-Forwarded-Proto $scheme;
                proxy_set_header X-Forwarded-Server $host;
                proxy_set_header X-Real-IP $remote_addr;

                # We add those 3 extra options for websockets proxying, see http                                       s://nginx.org/en/docs/http/websocket.html
                proxy_http_version 1.1;
                proxy_set_header Connection "Upgrade";
                proxy_set_header Upgrade $http_upgrade;
        }

       location / {
             proxy_pass http://127.0.0.1:1242;
        #        proxy_set_header Host 127.0.0.1; # Only if you need to override                                        default host
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
               proxy_set_header X-Forwarded-Host $host:$server_port;
                proxy_set_header X-Forwarded-Proto $scheme;
                proxy_set_header X-Forwarded-Server $host;
                proxy_set_header X-Real-IP $remote_addr;
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }

# Settings for a TLS enabled server.
#
#    server {
#        listen       443 ssl http2 default_server;
#        listen       [::]:443 ssl http2 default_server;
#        server_name  _;
#        root         /usr/share/nginx/html;
#
#        ssl_certificate "/etc/pki/nginx/server.crt";
#        ssl_certificate_key "/etc/pki/nginx/private/server.key";
#        ssl_session_cache shared:SSL:1m;
#        ssl_session_timeout  10m;
#        ssl_ciphers PROFILE=SYSTEM;
#        ssl_prefer_server_ciphers on;
#
#        # Load configuration files for the default server block.
#        include /etc/nginx/default.d/*.conf;
#
#        location / {
#        }
#
#        error_page 404 /404.html;
#            location = /40x.html {
#        }
#
#        error_page 500 502 503 504 /50x.html;
#            location = /50x.html {
#        }
#    }

}
```

